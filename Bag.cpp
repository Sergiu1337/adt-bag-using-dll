#include "Bag.h"
#include "BagIterator.h"
#include <exception>
#include <iostream>
using namespace std;


Bag::Bag() {
	this->count = 0;

	this->head = NULL;
	this->tail = NULL;
}


void Bag::add(TElem elem) {
	//TODO - Implementation
	Node* temp = new Node;
	temp->data = elem;

	if (this->count == 0) {
		temp->prev = NULL;
		this->head = temp;
		this->tail = temp;
	}
	else {
		temp->prev = this->tail;
		this->tail->next = temp;
		this->tail = temp;
	}

	this->count += 1;
	temp->nrOccurences = this->nrOccurrences(elem);
}


bool Bag::remove(TElem elem) {
	//TODO - Implementation

	if (this->count > 0) {
		Node* start = this->head;
		for (int i = 0; i < this->count; i++) {
			if (start->data == elem) {
				//Case 1 - it is at the beginning but there are other elements
				if (start->prev == NULL && start->next != NULL) {
					this->head = start->next;
				}
				//Case 2 - it is at the beginning and there are no other elements
				else if (start->prev == NULL && start->next == NULL) {
					this->head = NULL;
					this->tail = NULL;
				}
				//Case 3 - it is somewhere in between
				else if (start->next != NULL && start->prev != NULL) {
					start->prev->next = start->next;
				}
				//Case 4 - it is at the end
				else if (start->next == NULL && start->prev != NULL) {
					this->tail = start->prev;
					this->tail->next = NULL;
				}
				this->count -= 1;
				delete start;
				return true;
			}
			start = start->next;
		}
		return false;
	}
	else
		return 0;
}


bool Bag::search(TElem elem) const {
	//TODO - Implementation

	Node* start = this->head;
	for (int i = 0; i < this->count; i++) {
		if (start->data == elem) return true;
		start = start->next;
	}
	return false;
}

int Bag::nrOccurrences(TElem elem) const {
	//TODO - Implementation

	int counter = 0;

	Node* start = this->head;
	for (int i = 0; i < this->count; i++) {
		if (start->data == elem) counter++;
		start = start->next;
	}
	return counter;
}


int Bag::size() const {
	//TODO - Implementation

	return this->count;
}


bool Bag::isEmpty() const {
	//TODO - Implementation

	if (this->count == 0) {
		return true;
	}
	else {
		return false;
	}
}

BagIterator Bag::iterator() const {
	return BagIterator(*this);
}


Bag::~Bag() {
	//TODO - Implementation

	Node* next = NULL;
	Node* current = this->head;

	for (int i = 0; i < this->count; i++) {
		next = current->next;
		delete current;
		current = next;
	}

	this->head = NULL;
	this->tail = NULL;
}

