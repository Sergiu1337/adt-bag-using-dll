#include <exception>
#include "BagIterator.h"
#include "Bag.h"

using namespace std;

BagIterator::BagIterator(const Bag& c): bag(c)
{
	//TODO - Implementation

	this->current = 0;
	this->CurrentNode = bag.head;
}

void BagIterator::first() {
	//TODO - Implementation

	this->CurrentNode = bag.head;
	this->current = 0;
}


void BagIterator::next() {
	//TODO - Implementation

	if (this->valid()) {
		this->CurrentNode = this->CurrentNode->next;
		this->current++;
	}
	else throw exception();
}


bool BagIterator::valid() const {
	//TODO - Implementation

	if (this->current < bag.size())	return true;
	else return false;
}



TElem BagIterator::getCurrent() const
{
	if (this->CurrentNode != NULL) return this->CurrentNode->data;
	else throw exception();
}
